
## Database Tables ##

1. category
CREATE TABLE category (
    id int NOT NULL AUTO_INCREMENT,
    name varchar(255) NOT NULL,
    type varchar(255),
    PRIMARY KEY (id)
);

2. products

CREATE TABLE products (
    id int NOT NULL AUTO_INCREMENT,
    category_id int NOT NULL,
    name varchar(255),
    sku varchar(255),
    price int,
    image varchar(255),
	PRIMARY KEY (id),
	FOREIGN KEY (category_id) REFERENCES category(id)
);




