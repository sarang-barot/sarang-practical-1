var mysql = require('mysql');
var pool  = mysql.createPool({
  connectionLimit : 10,
  host            : 'localhost',
  user            : 'root',
  password        : 'smartant',
  database        : 'my_pract'
});

exports.getProducts = function(callback){
  pool.query('SELECT * from products', function (error, results, fields) {
    if (error) {
      callback([]);
      throw error;
    }else{
      callback(results);
    }
  });
};

exports.addProduct = function(params, callback) {
	new Promise((resolve, reject) => {
        pool.getConnection(function(err, connection) {
        if(err) { console.log(err); 
        	callback({result:"fail", msg:'Fail to add product'});
        	 }
          connection.query("INSERT INTO products set ? ",params, function(err, results) {
          if(err){
           console.log("Error Selecting : %s ",err );
           callback({result:"fail", msg:'Fail to add product'});
          }else{
           callback({result:"success", msg:'product added ok.'})
         }
      });
    });
    });
};

exports.deleteProduct =  function(id, callback){
  var sql = "delete FROM products where id='"+id+"'";
  pool.getConnection(function(err, connection) {
    if(err) { console.log(err); callback({result:"fail", msg:'Fail to delete product'});}
    // make the query
    connection.query(sql, function(err, results) {
      connection.release();
      if(err) { console.log(err); callback({result:"fail", msg:'Fail to delete product'}); }
      callback({result:"success", msg:'product deleted.'}) ;
    });
  });
};

exports.updateProduct = function(params, id, callback){
         pool.getConnection(function(err, connection) {
        if(err) { console.log(err); callback({result:"fail", msg:'Fail to update product'}); return; }
        connection.query("UPDATE products set ? WHERE id = ? ",[params, id], function(err, results) {
          if(err){
           console.log("Error Selecting : %s ",err );
           callback({result:"fail", msg:'Fail to update product'});
          }else{
           callback({result:"success", msg:'product updated.'});
         }
      });
    });
};
