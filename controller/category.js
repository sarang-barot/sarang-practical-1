var mysql = require('mysql');
var pool  = mysql.createPool({
  connectionLimit : 10,
  host            : 'localhost',
  user            : 'root',
  password        : 'smartant',
  database        : 'my_pract'
});

exports.getCategories = function(callback){
  pool.query('SELECT * from category', function (error, results, fields) {
    if (error) {
      callback([]);
      throw error;
    }else{
      callback(results);
    }
  });
};

exports.addCategory = function(params, callback) {
	new Promise((resolve, reject) => {
        pool.getConnection(function(err, connection) {
        if(err) { console.log(err); 
        	callback({result:"fail", msg:'Fail to add category'});
        	 }
          connection.query("INSERT INTO category set ? ",params, function(err, results) {
          if(err){
           console.log("Error Selecting : %s ",err );
           callback({result:"fail", msg:'Fail to add category'});
          }else{
           callback({result:"success", msg:'category added ok.'})
         }
      });
    });
    });
};

exports.deleteCategory =  function(id, callback){
  var sql = "delete FROM category where id='"+id+"'";
  pool.getConnection(function(err, connection) {
    if(err) { console.log(err); callback({result:"fail", msg:'Fail to delete category'});}
    // make the query
    connection.query(sql, function(err, results) {
      connection.release();
      if(err) { console.log(err); callback({result:"fail", msg:'Fail to delete category'}); }
      callback({result:"success", msg:'category deleted.'}) ;
    });
  });
};

exports.updateCategory = function(params, id, callback){
         pool.getConnection(function(err, connection) {
        if(err) { console.log(err); callback({result:"fail", msg:'Fail to update category'}); return; }
        connection.query("UPDATE category set ? WHERE id = ? ",[params, id], function(err, results) {
          if(err){
           console.log("Error Selecting : %s ",err );
           callback({result:"fail", msg:'Fail to update category'});
          }else{
           callback({result:"success", msg:'category updated.'});
         }
      });
    });
};
